# sample_projects

Some sample projects/code for display purposes. Most of it is from academic work since I cannot share the projects done professionaly for legal purposes. 

- connected_tile: Solving the connected tile problem using transfer learning. Master's Project. (Pytorch)
- TCN: Implmenatation of paper "An Empirical Evaluation of Generic Convolutional and Recurrent Networks
for Sequence Modeling" (Tensorflow)
- energy_forecast: Data wrangling for solar/wind energy forecast.
- XFEL: Prediction of XFEL output parameters using Machine Learning
- A.I Lab: Some small excersises for a course.
- comtech: some ML excersises for a course.
