# Deep Learning Lab

###  Project Task: Prediction of XFEL output parameters using Machine Learning 

##### File: Data sorting:  
Read database file without XTCAV images and store the data in the form of pandas dataframe. Columns in the dataframe contained the features and labels (230 + 64 + mean and standard_deviation of labels) corresponding to each experiment ID.  
With the keyword XTCAV you can access the XTCAV Image data for each of experiment ID if ID doesn't have XTCAV Image it was appended with zeros.  
With the keyword dropped_ids you can access the IDs which were dropped because in these IDs there was no feature data present.  
 
##### File: with_mean_std
Read features from database file along with mean and standard deviation. Apply deep neural network to predict mean and standard deviation

##### File: Regressor_tree  
Use Mean, Standard Deviation, ArgMax and Center of Mass for "intensity_photon_energy" and "intensity_time" as target variables in Gradient Boosting Regressor

##### File: All_Labels
Read all features and all labels (230+64) from database file, remove features like AMO:LMP:VG:20:PRESS which provide no information and train neural network to predict all labels.

##### File: Autoencoder
Load all features and apply autoencoder to reduce feature space and later  use Mean, Standard Deviation, ArgMax and Center of Mass for "intensity_photon_energy" and "intensity_time" as target variables for training and prediction

##### File: Weka_Plots  
Show predictions of M5P trained model which were trained using weka toolbox. Predicted and actual values of features were saved in CSV files in weka which were imported in notebook to make plots.



#### Methods Used for modeling and prediction:
1. Deep Neural Network
2. Deep Neural Network with Auto-encoder
2. M5P Model-trees
3. Gradient Boosting Regressor

