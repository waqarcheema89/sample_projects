# -*- coding: utf-8 -*-
"""
Created on Sat Jun  7 13:01:05 2020

@author: CHEEMA
"""

import tensorflow as tf
import  numpy as np

def compute_causal_padding(dilation_rate,kernel_size):
    left_pad = dilation_rate * (kernel_size-1)
    causal_padding = [[0,0],[left_pad,0],[0,0]]
    return causal_padding


def conv1d(inputs,filters,kernel_size,strides=1,padding='valid',
           dilation_rate=1,activation=None,use_bias=True, 
           bias_initializer='zeros',trainable=True,scope="conv1d"):
    
    in_channels = inputs.get_shape().as_list()[-1]
    kernel_shape = [kernel_size,in_channels,filters]
    weights_initializer = tf.contrib.layers.xavier_initializer()
    
    with tf.variable_scope(scope):
        weights = tf.get_variable('weights',shape=kernel_shape,
                                  initializer=weights_initializer,
                                  trainable=trainable)
        
        if padding.lower() == 'causal':
            paddings = compute_causal_padding(dilation_rate,kernel_size)
            inputs = tf.pad(inputs,paddings)
            padding ='valid'
            
        output = tf.nn.convolution(inputs,weights,strides=[strides],
                              padding=padding.upper(),
                              dilation_rate=[dilation_rate])
        
        if use_bias:
            bias = tf.get_variable('biases',shape=[filters],
                                   trainable=trainable)
            output = tf.nn.bias_add(output,bias)
        
        if activation != None:
            act_fn = getattr(tf.nn,activation)
            output = act_fn(output)
    
    macs= output.get_shape().as_list()[1]*np.prod(kernel_shape)
    print('MAC ops:',macs,'Kernel shape:',kernel_shape,'output shape:',output.get_shape().as_list())        
    return output

def dense(inputs,neurons,activation=None,use_bias=True, 
          trainable=True,scope="dense"):
    
    in_channels = inputs.get_shape().as_list()[-1]
    kernel_shape = [in_channels,neurons]
    weights_initializer = tf.contrib.layers.xavier_initializer()
    
    with tf.variable_scope(scope):
        weights = tf.get_variable('weights',shape=kernel_shape,
                                  initializer=weights_initializer,
                                  trainable=trainable)
        
        output = tf.matmul(inputs,weights)
        
        if use_bias:
            bias = tf.get_variable('biases',shape=[neurons],
                                   trainable=trainable)
            output = tf.nn.bias_add(output,bias)
        
        if activation != None:
            act_fn = getattr(tf.nn,activation)
            output = act_fn(output)
    
    macs= np.prod(kernel_shape)
    print('MAC ops:',macs) 
    return output

def residual_block(inputs, dilation_rate, nb_filters, kernel_size, padding,
                   block,activation='relu', dropout_rate=1.,
                   use_batch_norm=False, is_training=False):
    x = inputs
    for k in range(2):
        x = conv1d(x,nb_filters,kernel_size,padding=padding,
                   dilation_rate=dilation_rate,
                   scope='block_{}_conv1d_{}'.format(block,k+1))
        if use_batch_norm:
            x = tf.layers.batch_normalization(x,training=is_training,
                                             name='block_{}_bn_{}'.format(block,k+1))        
        x = tf.nn.relu(x)

    x_2 = conv1d(inputs, nb_filters, 1, padding='same',
                 scope='block_{}_conv1d_3'.format(block))

    x = tf.add(x_2,x)
    if activation != None:
        act_fn = getattr(tf.nn,activation)
        x = act_fn(x)
    x = tf.nn.dropout(x,dropout_rate)
    return x

def process_dilations(dilations):
    def is_power_of_two(num):
        return num != 0 and ((num & (num - 1)) == 0)

    if all([is_power_of_two(i) for i in dilations]):
        return dilations

    else:
        new_dilations = [2 ** i for i in dilations]
        return new_dilations

class TCN(object):
    def __init__(self,
                 nb_filters=64,
                 kernel_size=2,
                 nb_stacks=1,
                 dilations=(1, 2, 4, 8, 16, 32),
                 padding='causal',
                 dropout_rate=1.0,
                 activation=None,
                 use_batch_norm=False):
        self.dropout_rate = dropout_rate
        self.dilations = dilations
        self.nb_stacks = nb_stacks
        self.kernel_size = kernel_size
        self.nb_filters = nb_filters
        self.activation = activation
        self.padding = padding
        self.use_batch_norm = use_batch_norm
        
    def build(self,inputs,training=False,regression=True,num_classes=3):
        x = inputs
        
        x = tf.transpose(x,[0,1,3,2])
        x_shape = x.get_shape().as_list()
        x = tf.reshape(x,[-1,x_shape[1]*x_shape[2],x_shape[3]])
        x=conv1d(x,20,3,padding='valid',scope='conv1',strides=2)
        x = tf.layers.batch_normalization(x,training=training,
                                         name='bn1') 
        x = tf.nn.relu(x)
        block = 0
        for s in range(self.nb_stacks):
            for d in self.dilations:
                x = residual_block(x,d,self.nb_filters[block],self.kernel_size,
                                   self.padding,block,self.activation,
                                   self.dropout_rate,self.use_batch_norm,
                                   training)
                block += 1
        x = tf.reduce_mean(x,[1])
        x = tf.nn.dropout(x,self.dropout_rate)
        if not regression:
            x = dense(x,num_classes,scope='dense1')
        else:
            x = dense(x,50,scope='dense1')
            x = dense(x,1,scope='dense2')
        return x
    
def train_TCN(inputs,labels,epochs,batch_size,
              num_feat, nb_filters, kernel_size, dilations, 
              nb_stacks,max_len, padding='causal',regression=False, 
              dropout_rate=0.95, lr=0.002,use_batch_norm=False):
    dilations = process_dilations(dilations)
    train_inputs, test_inputs = inputs
    train_labels, test_labels = labels
    
    tf.reset_default_graph()
    
    input_layer = tf.placeholder(tf.float32,shape=[None,max_len,num_feat])
    labels = tf.placeholder(tf.float32,shape=[None,])
    labels_= tf.reshape(labels,[-1,1])
    dropout = tf.placeholder(tf.float32,shape=None)
    
    TCN_rul = TCN(nb_filters,kernel_size,nb_stacks,dilations,padding,
                  dropout,use_batch_norm=use_batch_norm)

    predictions = TCN_rul.build(input_layer,True,True)
    
    loss = tf.losses.mean_squared_error(labels_,predictions)
    accuracy = tf.reduce_mean(tf.abs(tf.subtract(labels_,predictions)))
    
    # optimizer = tf.train.AdamOptimizer(lr).minimize(loss)
    optimizer = tf.train.AdamOptimizer(learning_rate=lr)
    gvs = optimizer.compute_gradients(loss)
    capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in gvs]
    train_op = optimizer.apply_gradients(capped_gvs)

    train_batch = int(np.floor(np.shape(train_inputs)[0]/batch_size))
    test_batch = int(np.floor(np.shape(test_inputs)[0]/batch_size))
    
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())
    saver = tf.train.Saver()
    best_loss = 1000
    
    #writer = tf.summary.FileWriter('./logs/graphs', sess.graph) # For Tensorboard
    
    for epoch in range(epochs):
        total_train_loss = 0
        total_train_acc = 0
        print('epoch:',epoch)
        perm_idx = np.random.permutation(np.shape(train_inputs)[0])
        train_inputs = train_inputs[perm_idx]
        train_labels = train_labels[perm_idx]
        for batch in range(train_batch):
            idx = batch
            feed_dict = {input_layer:train_inputs[idx*batch_size:batch_size*(idx+1),:,:],
                         dropout:dropout_rate,
                         labels:train_labels[idx*batch_size:batch_size*(idx+1)]}
            sess.run(train_op,feed_dict=feed_dict)
            train_loss,train_acc,p = sess.run([loss,accuracy,predictions],
                                            feed_dict=feed_dict)
            #writer.add_summary(train_loss, batch) # Tensorboard
            total_train_loss += train_loss
            total_train_acc += train_acc
        total_train_loss /= train_batch
        total_train_acc /= train_batch
        print('train loss: {}, train accuracy: {}'.format(total_train_loss,total_train_acc))
    
        total_test_loss = 0
        total_test_acc = 0
        for batch in range(test_batch):
            idx = batch
            feed_dict = {input_layer:test_inputs[idx*batch_size:batch_size*(idx+1),:,:],
                         dropout:1.,
                         labels:test_labels[idx*batch_size:batch_size*(idx+1)]}
            test_loss,test_acc = sess.run([loss,accuracy],
                                            feed_dict=feed_dict)
            total_test_loss += test_loss
            total_test_acc += test_acc
        total_test_loss /= test_batch
        total_test_acc /= test_batch
        print('test loss: {}, test accuracy: {}'.format(total_test_loss,total_test_acc))
        if total_test_loss < best_loss:
            best_loss = total_test_loss
            saver.save(sess,'model.ckpt')
            print('model saved in model.ckpt')
    sess.close()


def test_TCN(inputs,labels,batch_size,
              num_feat, nb_filters, kernel_size, dilations, 
              nb_stacks,max_len, padding='causal',regression=False, 
              dropout_rate=0.95, lr=0.002,use_batch_norm=False):
    dilations = process_dilations(dilations)
    test_inputs = inputs
    test_labels = labels
    
    tf.reset_default_graph()
    
    input_layer = tf.placeholder(tf.float32,shape=[None,max_len,num_feat])
    labels = tf.placeholder(tf.float32,shape=[None,])
    labels_= tf.reshape(labels,[-1,1])
    
    TCN_rul = TCN(nb_filters,kernel_size,nb_stacks,dilations,padding,
                  1.0,use_batch_norm=use_batch_norm)

    predictions = TCN_rul.build(input_layer,True,True)
    
    loss = tf.losses.mean_squared_error(labels_,predictions)
    accuracy = tf.reduce_mean(tf.abs(tf.subtract(labels_,predictions)))
    
    test_batch = int(np.floor(np.shape(test_inputs)[0]/batch_size))
    
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())
    saver = tf.train.Saver()
    saver.restore(sess,'model.ckpt')
    
    total_test_loss = 0
    total_test_acc = 0
    test_predictions = []
    for batch in range(test_batch):
        idx = batch
        feed_dict = {input_layer:test_inputs[idx*batch_size:batch_size*(idx+1),:,:],
                     labels:test_labels[idx*batch_size:batch_size*(idx+1)]}
        test_loss,test_acc, preds = sess.run([loss,accuracy,predictions],
                                        feed_dict=feed_dict)
        test_predictions.append(preds)
        total_test_loss += test_loss
        total_test_acc += test_acc
    total_test_loss /= test_batch
    total_test_acc /= test_batch
    print('test loss: {}, test accuracy: {}'.format(total_test_loss,total_test_acc))
    sess.close()
    return test_predictions

