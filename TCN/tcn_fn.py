import tcn
import tensorflow as tf

def create_tcn_model(fingerprint_input, model_settings,
                     model_size_info,is_training):
  """Builds a TCN model.     
  """
  if is_training:
    dropout_prob = tf.placeholder(tf.float32, name='dropout_prob')
  else:
    dropout_prob = 1.0
  input_frequency_size = model_settings['dct_coefficient_count']
  input_time_size = model_settings['spectrogram_length']
  label_count = model_settings['label_count']
  fingerprint_4d = tf.reshape(fingerprint_input,
                              [-1, input_time_size, input_frequency_size, 1])
    
  
  tcn_ = tcn.TCN(nb_filters=(16,24,32),
                 kernel_size=6,
                 nb_stacks=1,
                 dilations=(1,2,4),
                 padding='causal',
                 dropout_rate=dropout_prob,
                 activation='relu',
                 use_batch_norm=True)
  
  logits = tcn_.build(fingerprint_4d,is_training,False,num_classes=label_count)
  if is_training:
    return logits, dropout_prob
  else:
    return logits