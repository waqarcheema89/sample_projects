import numpy as np
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt

# Create a surface plot and projected filled contour plot under it.
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

def get_poly(noise=.2):
    """ Returns a simple data set with targests

    Parameters:
    noise - adjust the noise in the generated data points.

    Returns:
    =======
    (x,y) - feature values x and coressponding targets y.

    """
    x = np.arange(0,1.1,.1)
    np.random.seed(45678)
    y = np.sin(x*2*np.pi) + np.random.normal(scale=noise, size=len(x))
    return x,y

def get_data31():
    np.random.seed(564782)
    return np.random.normal(loc=3.14,scale=0.42,size=100)

def get_coins(which):
    if which == 1:
        return np.asarray([1, 1, 1, 0])
    elif which == 2:
        return np.asarray([0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1])
    elif which == 3:
        return np.asarray([1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1 ,1 ])
    return "no! Make the exercise! Dont play with the functions!"

def polynomial_matrix(x,degree):
    """
    Calculate the polynomials for each element in given x.

    Q = [[ x**0, x**1, ..., x**degree ]]

    Returns:
    ========
    Designmatrix with polynomial features.
    """
    pf = PolynomialFeatures(degree)
    return pf.fit_transform(x.reshape((-1,1)))

def plot_distances(distf, base=np.array([0,0])):
    """
    Plot level contours of given distance function **distf** around the development point **base**

    Parameters:
    ===========
    distf - (x,y) -> d A function taking two arrays and returning a positive distance measure.

    base - base point to develop the contour lines around.
    """
    X, Y = np.mgrid[-3:3:0.1, -3:3:0.1]
    Z = np.zeros_like(X)

    for i,_ in enumerate(X):
        for j,_ in enumerate(Y):
            x = X[i,j]
            y = Y[i,j]
            pt = np.asarray([x,y])
            Z[i,j] = distf(pt, base)

    levels = np.arange(0,6,0.25)


    plt.figure()
    CS = plt.contour(X, Y, Z, levels=levels)
    plt.clabel(CS, inline=1, fontsize=10)
    plt.title('Spatial Distances')
    plt.grid(True)
    plt.show()
    pass


def plot_gaussian_3D(x, y, rv, zlim=[-0.15, 1.0]):
    position = np.empty(x.shape + (2,))
    position[:, :, 0] = x
    position[:, :, 1] = y

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_surface(x, y, rv.pdf(position), rstride=3, cstride=3,
                    linewidth=1, antialiased=True,
                    cmap=cm.viridis)

    cset = ax.contourf(x, y, rv.pdf(position), zdir='z',
    offset=-0.15, cmap=cm.viridis)

    # Adjust the limits, ticks and view angle
    ax.set_zlim(zlim[0], zlim[1])
    ax.set_zticks(np.linspace(zlim[0], zlim[1],10))
    ax.view_init(27, -21)

    plt.show()

def get_cancer():
    """
    Get an artifical data set containing samples of patients without and with cancer.

    Returns:
    ========
    (nocancer, cancer)
    """
    np.random.seed(5647839)
    return (np.random.normal(size=200,loc=7.0,scale=2), np.random.normal(size=50,loc=10.5,scale=1))

def flip_bits(array,p):
    out = array.copy()
    flips = np.asarray(np.random.choice([1,0],p=[p,1-p],size=len(array)),dtype=bool)
    out[flips] = np.logical_not(out[flips])
    return out

def binary_symmetric_channel(input_seq, channel=11):
    """
    Simulation of a WLAN communication channel. Some bit's might flip while transmitting them.

    Parameters:
    ===========
    input_seq - numpy 1d array with a sequence of 0s and 1s which are transmitted by the sender

    channel - the wlan channel to use for the transmission. **Note:** Channel 8 and 11 seem
        to be working, 5 is defect and the rest very noisy.

    Returns:
    ========
    sequence of bits (0s and 1s) received by the receiver.
    """
    if channel == 11:
        return flip_bits(input_seq, 0.25)
    elif channel == 8:
        return flip_bits(input_seq, 0.12)
    elif channel == 5:
        return np.zeros(len(input_seq), dtype=int)
    else:
        return flip_bits(input_seq, 0.5)
