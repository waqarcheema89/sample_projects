import numpy as np
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt

# Create a surface plot and projected filled contour plot under it.
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from scipy.stats import multivariate_normal

# for ridge regression
from sklearn.linear_model import Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn import datasets

def get_poly(noise=.2):
    """ Returns a simple data set with targests

    Parameters:
    noise - adjust the noise in the generated data points.

    Returns:
    =======
    (x,y) - feature values x and coressponding targets y.

    """
    x = np.arange(0,1.1,.1)
    np.random.seed(45678)
    y = np.sin(x*2*np.pi) + np.random.normal(scale=noise, size=len(x))
    return x,y

def get_data31():
    np.random.seed(564782)
    return np.random.normal(loc=3.14,scale=0.42,size=100)

def get_coins(which):
    if which == 1:
        return np.asarray([1, 1, 1, 0])
    elif which == 2:
        return np.asarray([0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1])
    elif which == 3:
        return np.asarray([1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1 ,1 ])
    return "no! Make the exercise! Dont play with the functions!"

def polynomial_matrix(x,degree):
    """
    Calculate the polynomials for each element in given x.

    Q = [[ x**0, x**1, ..., x**degree ]]

    Returns:
    ========
    Designmatrix with polynomial features.
    """
    pf = PolynomialFeatures(degree)
    return pf.fit_transform(x.reshape((-1,1)))

def plot_distances(distf, base=np.array([0,0])):
    """
    Plot level contours of given distance function **distf** around the development point **base**

    Parameters:
    ===========
    distf - (x,y) -> d A function taking two arrays and returning a positive distance measure.

    base - base point to develop the contour lines around.
    """
    X, Y = np.mgrid[-3:3:0.1, -3:3:0.1]
    Z = np.zeros_like(X)

    for i,_ in enumerate(X):
        for j,_ in enumerate(Y):
            x = X[i,j]
            y = Y[i,j]
            pt = np.asarray([x,y])
            Z[i,j] = distf(pt, base)

    levels = np.arange(0,6,0.25)


    plt.figure()
    CS = plt.contour(X, Y, Z, levels=levels)
    plt.clabel(CS, inline=1, fontsize=10)
    plt.title('Spatial Distances')
    plt.grid(True)
    plt.show()
    pass


def plot_gaussian_3D(x, y, rv, zlim=[-0.15, 1.0]):
    position = np.empty(x.shape + (2,))
    position[:, :, 0] = x
    position[:, :, 1] = y

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_surface(x, y, rv.pdf(position), rstride=3, cstride=3,
                    linewidth=1, antialiased=True,
                    cmap=cm.viridis)

    cset = ax.contourf(x, y, rv.pdf(position), zdir='z',
    offset=-0.15, cmap=cm.viridis)

    # Adjust the limits, ticks and view angle
    ax.set_zlim(zlim[0], zlim[1])
    ax.set_zticks(np.linspace(zlim[0], zlim[1],10))
    ax.view_init(27, -21)

    plt.show()

def get_cancer():
    """
    Get an artifical data set containing samples of patients without and with cancer.

    Returns:
    ========
    (nocancer, cancer)
    """
    np.random.seed(5647839)
    return (np.random.normal(size=200,loc=7.0,scale=2), np.random.normal(size=50,loc=10.5,scale=1))

def flip_bits(array,p):
    np.random.seed(5647839)
    out = array.copy()
    flips = np.asarray(np.random.choice([1,0],p=[p,1-p],size=len(array)),dtype=bool)
    out[flips] = np.logical_not(out[flips])
    return out

def binary_symmetric_channel(input_seq, channel=11):
    """
    Simulation of a WLAN communication channel. Some bit's might flip while transmitting them.

    Parameters:
    ===========
    input_seq - numpy 1d array with a sequence of 0s and 1s which are transmitted by the sender

    channel - the wlan channel to use for the transmission. **Note:** Channel 8 and 11 seem
        to be working, 5 is defect and the rest very noisy.

    Returns:
    ========
    sequence of bits (0s and 1s) received by the receiver.
    """
    if channel == 11:
        return flip_bits(input_seq, 0.25)
    elif channel == 8:
        return flip_bits(input_seq, 0.12)
    elif channel == 5:
        return np.zeros(len(input_seq), dtype=int)
    else:
        return flip_bits(input_seq, 0.5)

def get_unknown_mean(size=200):
    """
    Parameters:
    ===========
    size : Number of Random samples to draw

    Returns
    =======
    A Random sample with 'unknown mean' and given covariance
    [[6.0  ,  .73],
     [ .73 , 0.57]]

    """
    mu = np.asarray([0,0])
    Sigma = np.asarray([[6.0,.73],[.73,0.57]])
    X = multivariate_normal.rvs(mean=mu, cov=Sigma,size=size, random_state=56783)
    return X

def plot_unknown_mean_dist(mu0, Sigma0):
    """
    Plot probability distribution of prior for a 2D Gaussian with
    unknown mean.
    
    Parameters:
    ===========
    mu0 : Prior-mean
    
    Sigma0 : Prior-Covariance
    
    """

    xi, yi = np.mgrid[X[:,0].min():X[:,0].max():.1,X[:,1].min():X[:,1].max():.1]      
    p = multivariate_normal.pdf(np.vstack([xi.flatten(), yi.flatten()]).T, mean=mu0,cov=Sigma0)
    plt.contourf(xi, yi, p.reshape(xi.shape))
    plt.title("$\mu$-Posterior")
    plt.xlabel("Estimated x1")
    plt.ylabel("Estimated x2")
    plt.colorbar()
class ridge_regression:
    def __init__(self, degree, penalty):
        """ Returns a model object for ridge regression.

        call fit(X,y) on the returned object to fit your model and predict(X) to make predictions.


        Parameters:
        ===========
        degree - polynomial degress to be used for regression

        penalty - the penalty term to control the regression. Larger values enforce stronger regularization.
        """
        self.rr = make_pipeline(PolynomialFeatures(degree), Ridge(alpha=penalty))

    def fit(self, X, y):
        self.rr.fit(X.reshape(-1,1), y.reshape(-1,1))
        return self

    def predict(self, X):
        return self.rr.predict(X.reshape(-1,1)).reshape(-1)

def get_noisy_ridge(fold, size=40):
    """ Get noise samples for ridge regression (bias/variance)

    Parameters:
    ===========
    fold - the data set of random samples.

    size - number of samples to generate

    Returns:
    ========

    X, y - Randomly choosen X values and coressponding noisy y values.
    """
    np.random.seed(fold)
    x = np.random.uniform(low=0.0, high=14, size=size)
    _, y = get_clean_ridge(x)
    return x, y + np.random.normal(0,1,len(y))

def get_clean_ridge(X=None):
    """ Get the actual targets for given samples for ridge regression (bias/variance)

    Parameters:
    ===========
    X - inputs to calculate y values. If X=None this function will generate values in the interval [0,7].abs

    Returns:
    ========
    X, y - X values and coressponding y values (targets).

     """
    X = np.arange(0,14,0.1) if X is None else X
    return X, 10*np.sin(X) + 10 * np.cos(2*X)

def gaussian_matrix(X, locs=None, s=1):
    """
    Calculate the activations of Gaussian basis function (centered around the values given in locs) for each element in given x.

    Q = [[ RBF(x, loc1), RBF(x, loc2), ..., RBF(x, locn) ]]

    Parameters:
    ===========
    X - input data

    locs - vector containing locations.
            Default is: np.linspace(0,14,40) # roughly 4*pi

    s - standard deviation used for Guassian kernel.

    Returns:
    ========
    Designmatrix with Gaussian features.
    """
    mu = np.linspace(0,14, 40) if locs is None else locs
    K = lambda x, mu : np.exp( -(np.tile(x, len(mu)) - mu)**2/s**2 )
    return np.asarray([K(x,mu) for x in X])

def gaussian_ridge_fit(x, y, penalty, locs=None):
    t = np.matrix(y).T
    Xb = np.matrix(gaussian_matrix(x, locs=locs))
    M = Xb.T*Xb
    return (penalty*np.eye(len(M)) + M).I*Xb.T*t

def gaussian_ridge_predict(x, w, locs=None):
    degree = len(w)-1
    return np.asarray((w.T*np.matrix(gaussian_matrix(x,locs=locs)).T)).flatten()


def get_noisy_bayesian_poly(n_samples=20, seed=42, s=1.0):
    """
    Get noisy samples for polynomial regression.

    Parameters:
    ===========
    n_samples - number of samples.

    seed - Random seed used for drawing random samples.
            Default is: 42

    s - standard deviation used for Gaussian noise.

    Returns:
    ========
    X, y - X values and coressponding y values (targets).
    """
    f = lambda x: np.sin(0.9 * x).flatten()

    #sample uniformly from space
    X = np.random.uniform(-5, 5, size=(n_samples))
    y = f(X) + s * np.random.randn(n_samples)

    return X, y


def make_meshgrid(x, y, h=.02):
    """Create a mesh of points to plot in

    Parameters
    ----------
    x: data to base x-axis meshgrid on
    y: data to base y-axis meshgrid on
    h: stepsize for meshgrid, optional

    Returns
    -------
    xx, yy : ndarray
    """
    x_min, x_max = x.min() - 1, x.max() + 1
    y_min, y_max = y.min() - 1, y.max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    return xx, yy

def plot_contours(clf, xx, yy, **params):
    """Plot the decision boundaries for a classifier.

    Parameters
    ----------
    ax: matplotlib axes object
    clf: a classifier
    xx: meshgrid ndarray
    yy: meshgrid ndarray
    params: dictionary of params to pass to contourf, optional
    """
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    out = plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)
    return out


def get_classification_data():
    n_samples = 100
    X = np.linspace(-2, 5, n_samples)
    y = np.zeros(n_samples)
    y[(X >  0) & (X < 2)] = 1
    return X.reshape((-1,1)), y

def get_kernelized_ls_ds():
    """ Get data for kernelized least squares.

    Returns:
    ========
    (X,y,f) Noisy training data and labels. **f** is the generator function.
    """
    np.random.seed(98)
    N = 25
    s=.2
    f = lambda x: np.sin(0.9*x).flatten()
    X = np.random.uniform(-5, 5, size=N)
    y = f(X) + s*np.random.randn(N)
    return (X,y,f)

def get_dna_sequences():
    """ Get pseudo dna sequences
    
    Returns:
    ========
    (X, y, X_test, y_test) returns training and test data including labels.
     """

    X = ["TCCGTTACCGTGTAACCGACGCCAAGACCGAG",\
         "GCTTGTAATCTGTTTTGACGCCCTCGTCCATGATG",\
         "AGTCAAGCTTTGGTTATTTGTGGTGTTCAATA",\
         "ACCTGAATCTAGTAACCGACGGCGTGTACGATA",\
         "CTTAAAACCTGTTATTAATGCGGTAAAAT",\
         "CCTGCATGCGTGGAAGTGACGCCTATCCAT",\
         "TGTTATACCTTTGGATCATTATTGAGTGAAA",\
         "CTTCAACGCCTATTATTTTGGGTGTGAAAT",\
         "ACCGTAAGTGTTACAGACCGTAAGTACCGTTG",\
         "CGTTCAACGCCTAGGTGTATTGGGTGTCTG"]
    y = [1, 1, -1, 1, -1, 1, -1, -1, 1, -1]
    X_test = ["CTTTCAAGGCCCTAATTATCATTGGGTTGAAT", "CGTACCACCCTTCGTTGATGTTTCCGGGAAT", "TCGTACTGCGAACTGAGCCTAGTGACCGACG"]
    y_test = [-1, 1, 1]
    return (X, y, X_test, y_test)

def get_svm_iris():
    """ Get a modifed version of the iris classification data set for SVM exercices
    
    Returns:
    ========
    quadrupel: ( X_train, y_train, X_test, y_test )
     """
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target

    X = X[:, :2]
    y[y == 2] = 0

    n_sample = len(X)

    np.random.seed(0)
    order = np.random.permutation(n_sample)
    X = X[order]
    y = y[order].astype(np.float)

    X_train = X[:int(.9 * n_sample)]
    y_train = y[:int(.9 * n_sample)]
    X_test = X[int(.9 * n_sample):]
    y_test = y[int(.9 * n_sample):]

    return X_train, y_train, X_test, y_test