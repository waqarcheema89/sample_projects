import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.metrics import roc_curve, auc
import os

WALL = -1
PATH = 1
VISITED = 3

def append_string(file_to_append, string):
    with open(os.path.expanduser(file_to_append), "a") as my_file:
        my_file.write(string)

def get_field_size(tile_name):
    if  tile_name[1].isalpha():
        return int(tile_name[0])
    else:
        return int(tile_name[0:2])

def prepare_data(file_name, field_size, nr_of_train_data=None):
    nr_features = (field_size * field_size)

    df = pd.read_csv(file_name, header=None, sep='\t')

    if nr_of_train_data is None:
        train, valid = train_test_split(df, train_size=0.8)
    else:
        train, valid = train_test_split(df, train_size=int(nr_of_train_data*1.25))

    train, test = train_test_split(train, train_size=0.8)

    train_dataset = train.ix[:, 0:(nr_features-1)]
    train_labels = train.ix[:, nr_features]

    test_dataset = test.ix[:, 0:(nr_features-1)]
    test_labels = test.ix[:, nr_features]

    #test_dataset = test.ix[:, 0:(nr_features-1)]
    #test_labels = test.ix[:, nr_features]

    train_dataset, train_labels = reformat(train_dataset, train_labels)
    #correct_it(train_dataset, train_labels, field_size)
    #valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
    #correct_it(valid_dataset, valid_labels, field_size)
    test_dataset, test_labels = reformat(test_dataset, test_labels)
    #correct_it(test_dataset, test_labels, field_size)

    return train_dataset, train_labels, test_dataset, test_labels

def reformat(dataset, labels, rnn=False):
  dataset = dataset.as_matrix().astype(np.float32)
  # Map 2 to [0.0, 1.0, 0.0 ...], 3 to [0.0, 0.0, 1.0 ...]
  labels = labels.as_matrix().astype(np.float32)
  if rnn:
    dataset = np.reshape(dataset, dataset.shape + (1,))
  return dataset, labels


# we need to find a path through the black fields(ones)
def is_vertically_and_horizontal_connected(matrix, verbose=False):
    field = matrix.copy()
    field_size = len(field)
    res = 0
    # horizontal check
    for i in range(field_size):
        if check_tiles(field, i, 0, vertical=False, verbose=verbose):
            res = 1
            break
            
    field = matrix.copy()        
    # vertical start
    for i in range(field_size):
        if check_tiles(field, 0, i, vertical=True, verbose=verbose):
            res += 1
            
    return res


def check_tiles(field, x, y, vertical=True, verbose=False):
    field_size = len(field)
    if field[x][y] == WALL:
        if verbose:
            print('wall at %d,%d' % (x,y))
        return False
    elif x == field_size-1 and vertical:
        if verbose:
            print ('found vertical at %d,%d' % (x, y))
        return True
    elif y == field_size-1 and not vertical:
        if verbose:
            print ('found horizontal at %d,%d' % (x, y))
        return True
    elif field[x][y] == VISITED:
        if verbose:
            print ('visited at %d,%d' % (x, y))
        return False
    
    if verbose:
        print ('visiting %d,%d' % (x, y))

    # mark as visited
    field[x][y] = VISITED

    # explore neighbors clockwise starting by the one on the right
    if (   (x < field_size-1 and check_tiles(field, x+1, y, vertical ,verbose))
        or (y < field_size-1 and check_tiles(field, x, y+1, vertical, verbose))
        or (y > 0 and check_tiles(field, x, y-1, vertical, verbose))
        or (x > 0 and check_tiles(field, x-1, y, vertical, verbose))):
        return True

    return False


def correct_it(train_dataset, train_labels, field_size):
    # store (id, correct label)
    wrong_label_id = []
    correct_label = []
    remove_label = []

    for cur_field_id in range(0, len(train_dataset)):
        cur_field = train_dataset[cur_field_id]
        cur_field = cur_field.reshape(field_size,field_size)
        # correct label in case it is connected, but marks think claims it is not connected
        if is_vertically_and_horizontal_connected(cur_field)==2 and train_labels[cur_field_id] != 1:
            wrong_label_id.append(cur_field_id)
            correct_label.append(1)
        # correct label in case it is not connected, but marks think claims it is connected
        if is_vertically_and_horizontal_connected(cur_field)==0 and train_labels[cur_field_id] != -1:
            wrong_label_id.append(cur_field_id)
            correct_label.append(-1)
        # remove all that are only patial connected
        elif is_vertically_and_horizontal_connected(cur_field)==1:
            remove_label.append(cur_field_id)
            
    correct_wrong_labels(train_labels, wrong_label_id, correct_label)
    remove_partially_connected(train_dataset, remove_label, train_labels)


def remove_partially_connected(train_dataset, remove_label, train_labels):
    ''' make sure this is executed after correct_wrong labels '''
    mask = np.ones(len(train_dataset), dtype=bool)
    mask[[remove_label]] = False
    return train_dataset[mask], train_labels[mask]


def correct_wrong_labels(train_labels, wrong_label_id, correct_label):
    for i in range(0, len(wrong_label_id)):
        train_labels[wrong_label_id[i]] = correct_label[i]
    
    return train_labels



