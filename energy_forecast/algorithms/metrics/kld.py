
import scipy
import numpy as np
from algorithms.metrics.utils import create_integral_points

def kld(p, q, symetric=False, N=1000, cdf=True):



    # removes columns with probabilities
    p_x = scipy.delete(p, 0, 1)
    p = p[:, 0]

    q_x = scipy.delete(q, 0, 1)
    q = q[:, 0]
    minval = 1.e-48
    d_kl = lambda p, q: p.clip(min=minval) * np.log(p.clip(min=minval) / q.clip(min=minval))

    # symmetric and nonnegative  $D_{\mathrm{KL}}(P\|Q) + D_{\mathrm{KL}}(Q\|P) $
    d_kl_s = lambda p, q: d_kl(p, q) + d_kl(q, p)

    # get min and max to define start and end point of integral
    a = min(np.min(p_x), np.min(q_x))
    b = max(np.max(p_x), np.max(q_x))

    int_eval_points = create_integral_points(a, b, N)

    # assure that both forecasted for the same number of values
    if p_x.shape[1] != q_x.shape[1]:
        raise Exception('Uneqaul nr of predictions.')

    # evaluate kld at each observation given within the probabilistic regression
    kld_values = []
    for i in range(0, p_x.shape[1]):
        p_x_i = p_x[:, i]
        q_x_i = q_x[:, i]

        # use piecewise linear fit to obtain probabilities of eval points for integration
        p_s = np.interp(int_eval_points, p_x_i, p, left=1.e-24, right=1)
        q_s = np.interp(int_eval_points, q_x_i, q, left=1.e-24, right=1)

        # conversion from cdf to pdf
        if cdf:
            p_s = p_s[1:] - p_s[:-1]
            p_s = np.insert(p_s,0,0)
            q_s = q_s[1:] - q_s[:-1]
            q_s = np.insert(q_s, 0, 0)

        if symetric:
            cur_dkl = d_kl_s(p_s, q_s)
        else:
            cur_dkl = d_kl(p_s, q_s)

        cur_dkl = np.sum(cur_dkl) * (b - a) / N

        kld_values.append(cur_dkl)

    return np.nanmean(kld_values)




def do_it():
    from numpy import log, exp, sqrt, pi
    import pandas as pd
    LOG_ZERO = np.nan
    elog = lambda X: np.array([log(x) if x > 0.1e-350 else LOG_ZERO for x in X])
    eexp = lambda X: np.array([exp(x) if x is not LOG_ZERO else 0 for x in X])

    N = lambda x, m, s: (1 / sqrt(2 * pi * s * s)) * exp(-((x - m) * (x - m)) / (2 * s * s))

    def realfilter(x):
        x[np.logical_not(np.isfinite(x))] = 0
        return x
    m_p = 0
    s_p = 1
    m_q = 1
    s_q = 1

    X = np.linspace(-10, 10, 10000)

    px = N(X, m_p, s_p)
    qx = N(X, m_q, s_q)
    ln_pq = px * (elog(px) - elog(qx))
    ln_pq = realfilter(ln_pq)

    p_x = pd.DataFrame({'P_Y': px, 'Y': X})
    q_x = pd.DataFrame({'P_Y': qx, 'Y': X})

    print('kld:', kld(p_x.as_matrix(), q_x.as_matrix(), cdf=False))

    X = np.linspace(-35, 35, 1000)

    gmm1d = [(0.5, 0, 1), (0.5, 0, 4)]
    gmm2d = [(.5, 10, 1), (.5, 10, 1)]

    gmm1 = lambda X: sum(map(lambda t: t[0] * N(X, t[1], t[2]), gmm1d))
    gmm2 = lambda X: sum(map(lambda t: t[0] * N(X, t[1], t[2]), gmm2d))

    p_x = pd.DataFrame({'P_Y': gmm1(X), 'Y': X})
    q_x = pd.DataFrame({'P_Y': gmm2(X), 'Y': X})

    print('kld:', kld(p_x.as_matrix(), q_x.as_matrix(), cdf=False))
    print('kld2:', kld(p_x.as_matrix(), q_x.as_matrix(), cdf=False, symetric=True))

if __name__ == "__main__":
    do_it()
