import numpy as np


def get_normalized_error(y, y_hat, norm_type):
    if norm_type == "nominal_capacity":
        # TODO: Do we need the real nominal capacity??
        return error(y, y_hat) / max(y)
    elif norm_type == "current_power_generation":
        return error(y, y_hat) / y
    elif norm_type == "deviation_from_average":
        y_mean = np.mean(y)
        return error(y, y_hat) / np.abs(y - y_mean)
    elif norm_type == "dynamic_characteristics":
        diff = -np.diff(y)
        # TODO: Should we remove the last element instead?
        return error(y.iloc[1:], y_hat.iloc[1:]) / diff
    else:
        raise ValueError


def error(y,y_hat):
    return y_hat - y

def delta(y):
    d = -np.diff(y)
    d = np.append(d, d[-1])
    return d


def bias(y, y_hat):

    N_k = len(y)
    return 1 / N_k * np.sum(error(y, y_hat))


def mae(y, y_hat, e=None):


    if e is None:
        e = error(y, y_hat)

    N_k = len(e)
    return 1 / N_k * np.sum(np.abs(e))


def mse(y, y_hat, e=None):

    if e is None:
        e = error(y, y_hat)
    N_k = len(e)
    return 1 / N_k * sum(e ** 2)


def rmse(y, y_hat):

    return np.sqrt(mse(y, y_hat))


def NBias(y, y_hat, y_inst):
    return bias(y, y_hat) / y_inst


def NMAE(y, y_hat, y_inst):
    return mae(y, y_hat) / y_inst


def NMSE_C(y, y_hat, y_inst):
    return mse(y, y_hat) / y_inst


def MRE(y, y_hat):
    e = error(y, y_hat) / y
    return mae(None, None, e)


def MAPE(y, y_hat):
    return MRE(y, y_hat) * 100


def NMSE(y, y_hat):
    N_k = len(y)
    y_mean = np.mean(y)
    return 1 / N_k * np.sum(error(y, y_hat) ** 2 / np.abs(y - y_mean))

def mRSE(y, y_hat):
    delta_y = delta(y)
    N_k = len(y)
    y_mean = np.mean(y)
    e = 1 / N_k * np.sum( error(y, y_hat) ** 2 \
                         /
                         (delta_y ** 2 + 1 / N_k * np.sum( (y - y_mean) **2) ))
    return np.sqrt(e)

def KL(y, y_hat):
    N_k = len(y)
    y_mean = np.mean(y)
    e = 1/N_k * np.sum( (error(y, y_hat) ** 2)
                        /
                        (1/N_k * np.sum((y - y_mean) ** 2)))
    return np.sqrt(e)

def RAE(y, y_hat):
    delta_y = delta(y)
    e = np.sum(np.abs(error(y, y_hat))) / \
        np.sum(np.abs(delta_y))
    return  e

def MASE(y, y_hat):
    N_k = len(y)
    e = (1/N_k/(N_k-1)) * np.sum(np.abs(error(y, y_hat))) /\
                        np.sum(np.abs(-np.diff(y)))
    return e

def RSE(y,y_hat):
    delta_y = delta(y)
    e = np.sum(error(y,y_hat) ** 2 / delta_y ** 2)
    return np.sqrt(e)

def SDE(e, q):
    e_mean = np.mean(e)
    N_k = len(e)
    e = np.sumnp.sum(e-e_mean) / (N_k - (q+1))

def Imp(e_base, e_eval):
    return (e_base - e_eval) / e_base

def R_squared(y, y_hat):
    y_mean = np.mean(y)
    y_hat_mean = np.mean(y_hat)
    numerator = np.sum((y_hat-y_hat_mean)*(y-y_mean)) ** 2
    denominator = np.sum(y_hat-y_hat_mean)**2 * np.sum(y-y_mean) ** 2
    return numerator / denominator

def theil_U(y,y_hat):


    #numerator = np.sum()c
    # right shift,to shift the last present y value "below" y_hat
    y_guess = y[1:]
    #
    numerator = np.sum((y_hat[0:-1] - y[0:-1]) / y_guess) ** 2
    denominator = np.sum((y[0:-1]-y_guess) / y_guess)**2
    return  np.sqrt(numerator / denominator)

def test():
     y = np.array([1,2,3])
     y_hat = np.array([1.1, 2.1, 3.2])
     print(error(y,y_hat))
     print(delta(y))
     print(bias(y, y_hat))
     print(mae(y, y_hat))
     print(mse(y, y_hat))
     print(rmse(y, y_hat))
     print(NBias(y, y_hat, 3))
     print(NMAE(y, y_hat,3))
     print(NMSE_C(y, y_hat, 3))
     print(MRE(y, y_hat))
     print(MAPE(y, y_hat))
     print(NMSE(y, y_hat))
     print(mRSE(y, y_hat))
     print(KL(y, y_hat))
     print(RAE(y, y_hat))
     print(MASE(y, y_hat))
     print(RSE(y, y_hat))
     print(Imp(y, y_hat))
     print(R_squared(y, y_hat))
     print(theil_U(y, y_hat))


