import numpy as np

def create_integral_points(a, b, N):
    t = np.linspace(a + (b - a) / (2 * N), b - (b - a) / (2 * N), N)
    return t
	