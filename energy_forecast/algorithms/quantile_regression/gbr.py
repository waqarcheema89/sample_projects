
import numpy as np
from sklearn.ensemble import GradientBoostingRegressor

class GBR:
    def __init__(self, quantiles=None, quantile_dist=0.01):

        self.X = None
        self.y = None
        self.models = []
        self.quantiles = quantiles

        if self.quantiles is None:
            self.quantiles = np.arange(quantile_dist, 1.0, quantile_dist)

    def fit(self, X , y):


        if X is None or y is None:
            print('Argument was None.')
            return self

        self.X = X
        self.y = y

        self.models, self.quantiles = self.__calc_gradient_boosting_reg()

        return self.models

    def predict(self, X):


        # length + 1 for prob column
        C = np.zeros((len(self.models), len(X) + 1))

        for i in range(0,len(self.models)):
            cur_model = self.models[i]
            cur_prob = self.quantiles[i]
            C[i][0] = cur_prob
            for j in range(1, len(X) + 1):
                cur_x = np.array(X[j-1]).reshape(1, -1)
                prediction = cur_model.predict(cur_x)
                C[i][j] = prediction

        return C

    def __calc_gradient_boosting_reg(self, seq=0.01):
        models = []
        for q in self.quantiles:
            alpha = q
            _gbr = GradientBoostingRegressor(loss='quantile', alpha=alpha,
                                             n_estimators=250, max_depth=3,
                                             learning_rate=.1, min_samples_leaf=9,
                                             min_samples_split=9)

            _gbr.fit(self.X.values.reshape(-1, 1), self.y.ravel())
            models.append(_gbr)

        return models, self.quantiles
