
import numpy as np
import statsmodels.formula.api as smf

from algorithms.utils import pandas as pd


class LinearQR:
    def __init__(self, quantiles=None, quantile_dist=0.01):

        self.X = None
        self.y = None
        self.models = []
        self.quantiles = quantiles

        if self.quantiles is None:
            self.quantiles = np.arange(quantile_dist, 1.0, quantile_dist)

    def fit(self, X, y):


        if X is None or y is None:
            print('Argument was None.')
            return self

        self.X = X
        self.y = y

        self.models, self.quantiles = self.__calc_simple_quantile_reg()

        return self.models

    def predict(self, X):


        C = np.zeros((len(self.models), len(X) + 1))

        for i in range(0, len(self.models)):
            cur_model = self.models[i][1]
            cur_prob = self.models[i][0]
            C[i][0] = cur_prob
            for j in range(1, len(X) + 1):
                cur_x = X[j - 1]
                prediction = cur_model.predict(pd.DataFrame({'X': [cur_x]}))
                C[i][j] = prediction

        return C

    def __calc_simple_quantile_reg(self, seq=0.01):
        df = pd.DataFrame({'X': self.X, 'Y': self.y})

        # ecluding bias
        formula = 'Y ~ X - 1'

        mod = smf.quantreg(formula, data=df)

        def fit_model(q):
            res = mod.fit(q=q)
            return [q, res]

        models = [fit_model(x) for x in self.quantiles]

        return models, self.quantiles
