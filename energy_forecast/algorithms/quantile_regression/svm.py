from sklearn.base import clone
from sklearn.base import BaseEstimator
from algorithms.metrics.propabilistic_errors import crps_fast
from liquidSVM import *
import numpy as np


class SVM_QR(BaseEstimator):
    def __init__(self, quantiles=[0.05, 0.25, 0.5, 0.75, 0.95], min_value=0, max_value=1):
        self.quantiles = quantiles
        self.min_value = min_value
        self.max_value = max_value

    def fit(self, X, y):
        self.model = qtSVM(X, y,
                           weights=self.quantiles,
                           display=True,
                           threads=0,  
                           scale=False,  
                           partition_choice=6,  
                           adaptivity_control=3, 
                           random_seed=42,
                           folds=1,
                           GPUs=1,
                           useCells=True)

    def predict(self, X):
        return Exception('Function not defined.')

    def predict_proba(self, X):

        #check_is_fitted(self, ['data_',])
        #X = check_array(X)
        #X = Bunch({'data': X})

        quantiles = np.hstack(([0], self.quantiles, [1])).reshape(
            len(self.quantiles) + 2, 1)
        prediction = self.model.predict(X)

        predictions = prediction.T
        nr_data = predictions.shape[1]
        mins = (np.ones(nr_data) * self.min_value).reshape(1, nr_data)
        maxs = (np.ones(nr_data) * self.max_value).reshape(1, nr_data)
        predictions = np.concatenate((mins, predictions, maxs), axis=0)
        predictions = np.concatenate((quantiles, predictions), axis=1)

        return predictions

    def score(self, X, y):
        return crps_fast(y, self.predict_proba(X))
