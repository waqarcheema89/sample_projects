import matplotlib.pyplot as plt
from callbacks_template import Callback


class LRFinder(Callback):
    '''
    
    Adapted from https://github.com/LucasAnders1/LearningRateFinder/blob/master/lr_finder_callback.py for Pytorch
    

    '''

    def __init__(self, optimizer, max_batches = 1000, base_lr=0.0001, max_lr=0.1, lr_step_size=0.001):

        self.max_batches = max_batches
        self.base_lr = base_lr
        self.max_lr = max_lr
        self.lr_step_size = lr_step_size
        self.optimizer = optimizer
        self.losses = []
        self.lrs = []
        self.lr = 0
        self.batches = 0
        self.stop_training = False



    def on_train_begin(self):
        self.set_lr(self.base_lr)

    def on_batch_end(self, loss):
        self.lrs.append(self.lr)
        self.losses.append(loss.cpu().data.numpy())

        if self.batches >= self.max_batches or self.lr >= self.max_lr:
            self.stop_training = True

        self.lr = self.base_lr + self.batches * self.lr_step_size

        # Set new learning rate of optimizer
        self.set_lr(self.lr)

        self.batches += 1

        return self.stop_training

    def set_lr(self, lr):
        self.current_lr = lr

        for g in self.optimizer.param_groups:
            g['lr'] = lr

    def on_train_end(self):
        plt.plot(self.lrs, self.losses)
        plt.xscale('log')
        plt.yscale('log')
