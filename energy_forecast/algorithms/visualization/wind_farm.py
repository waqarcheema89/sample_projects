import numpy as np
from matplotlib import pyplot as plt
from windrose import WindroseAxes


def wind_scatter_plot(x, y, show=True, xlabel=None, ylabel=None):
    plt.figure(figsize=(10, 10))
    p1 = plt.scatter(x, y, alpha=0.6, marker='o')

    if xlabel is not None:
        plt.xlabel(xlabel)
    else:
        plt.xlabel('WindSpeed')

    if ylabel is not None:
        plt.ylabel(ylabel)
    else:
        plt.ylabel('Power')

    plt.title('Power Forecast')
    if show:
        plt.show()
    return p1


def wind_rose_plot(df, zonal_wind, meridional_wind, wind_speed=None):


    if wind_speed is not None:
        v = df[wind_speed] * df[zonal_wind] * np.pi
        u = df[wind_speed] * df[meridional_wind] * np.pi
    else:
        v = df.loc[:, meridional_wind] * np.pi
        u = df.loc[:, zonal_wind] * np.pi

    ws = np.sqrt((u * u) + (v * v))
    d = np.arctan2(u, v)
    d = (270 - (d * (180 / np.pi))) % 360
    ws[ws > 70] = 70
    ax = WindroseAxes.from_ax()
    ax.bar(d, ws, normed=True, opening=0.8, edgecolor='white')
    ax.set_legend()
    plt.show()
