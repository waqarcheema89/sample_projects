import matplotlib.pylab as plt
import numpy as np


def cdf(y, probs, xlabel):
    plt.figure(figsize=(10, 10))
    plt.scatter(y, probs)
    plt.plot(y, probs)
    plt.xlabel(xlabel)
    plt.ylabel('Cumulative Probability Density P(y)')
    plt.show()


def plot_pdf(ys, probs):
    p_l = np.array((np.diff(np.append(probs, [1]))))
    ys = np.concatenate(([0], ys, [1]))
    p_l = np.concatenate(([p_l[0]], p_l, [p_l[-1]]))
    plt.step(ys, ys/p_l, color='black')
    plt.vlines(ys, 0, ys/p_l)
    plt.fill()
    plt.show()

