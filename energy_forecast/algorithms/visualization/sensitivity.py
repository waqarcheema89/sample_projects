import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
sns.set_context('paper')
sns.set_palette("cubehelix", 10)
sns.set(font_scale=1.2)


def _drop_na_values(df):
    df = df.copy().dropna(how='all', axis=0)
    ids = df.index
    df = df.drop([i for i in df.columns if i not in ids], axis=1)

    return df


def _fillnas_and_sort(df):


    def count_na(x): return x.isnull().sum(axis=0)

    nas = count_na(df)
    df_tr = df[list(nas.sort_values().index)]
    df_tr = df_tr.fillna(value=1e-18)

    return df_tr


def plot_sobol_heatmap(s2_df, model_name, vmin=None, vmax=None, annot=False, dropna=False):
    if dropna:
        s2_df = _drop_na_values(s2_df)
    if dropna and isinstance(annot, pd.DataFrame):
        annot = _drop_na_values(annot)

    plt.figure(figsize=(8, 6))
    ax = plt.axes()
    sns.heatmap(s2_df, ax=ax, vmin=vmin, vmax=vmax, annot=annot, )
    ax.set_title(r'$S_2$ for ' + model_name + ' model')
    plt.show()
