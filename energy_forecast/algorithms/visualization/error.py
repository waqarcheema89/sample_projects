import matplotlib.pylab as plt
import seaborn as sns
import numpy as np
import pandas as pd
from alogirhtms.utils.data import flatten_list
from alogirhtms.utils.data import extend_list




def plot_error_scores(error_scores, names_errors, names_model):


    ne = []
    nm = []

    if len(error_scores) == len(names_errors):
        for idx in range(len(error_scores)):
            for idy in range(len(error_scores[idx])):
                cur_ne = extend_list(names_errors[idx], len(error_scores[idx][idy]))
                ne.append(cur_ne)

                cur_nm = [names_model[idy] for _ in range(len(error_scores[idx][idy]))]
                nm.append(cur_nm)


        df = pd.DataFrame({'Error': flatten_list(flatten_list(error_scores)),
                           'names_errors': flatten_list(ne),
                           'names_model': flatten_list(nm)})

        sns.boxplot(x='names_errors', y='Error', hue='names_model', data=df)
        plt.xlabel('Error Score')
        plt.ylabel('Error Score Value')
        plt.show()
    else:
        raise ValueError

    return df


def plot_model_improvement(error_scores_1, error_scores_2, names_errors):


    ne = []
    errors = []

    if len(error_scores_1) == len(names_errors) == len(error_scores_2):
        for idx in range(len(error_scores_1)):

            cur_ne = extend_list(names_errors[idx], len(error_scores_1[idx]))
            ne.append(cur_ne)
            errors.append(error_scores_1[idx] - error_scores_2[idx])

        df = pd.DataFrame({'Error': flatten_list(errors),
                           'names_errors': flatten_list(ne)})

        sns.boxplot(x='names_errors', y='Error', data=df)
        plt.xlabel('Error Score')
        plt.ylabel('Improvement')
        plt.show()
    else:
        raise ValueError

    return df
