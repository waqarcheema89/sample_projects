import pandas as pd

from sklearn.base import BaseEstimator, TransformerMixin


class SortByColumns(BaseEstimator, TransformerMixin):


    def __init__(self, columns_to_sort=None, set_index=False):
        self.columns_to_sort = columns_to_sort
        self.set_index = set_index

    def fit(self, X, y=None):

        # check if X is pandas DataFrame
        if not isinstance(X, pd.DataFrame):
            raise ValueError("Input data is no valid pandas DataFrame")
        return self

    def transform(self, X, y=None):


        # check if X is pandas DataFrame
        if not isinstance(X, pd.DataFrame):
            raise ValueError("Input data is no valid pandas DataFrame")
            
        transformed_data = X
        
        if self.columns_to_sort is not None:
            if self.set_index : transformed_data = transformed_data.set_index(self.columns_to_sort)
            transformed_data.sort_index(inplace=True)
            #transformed_data = transformed_data.sort_values(self.columns_to_sort)
        
        return transformed_data
