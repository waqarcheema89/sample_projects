import pandas as pd

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import MinMaxScaler

class PowerNormalization(BaseEstimator, TransformerMixin):


    def __init__(self,
                 max_power='MaxPowerGeneration',
                 current_power='PowerGeneration'
                 ):
        self.max_power = max_power
        self.current_power = current_power

    def fit(self, X, y=None):

        # check if X is pandas DataFrame
        if not isinstance(X, pd.DataFrame):
            raise ValueError("Input data is no valid pandas DataFrame")
        return self

    def transform(self, X, y=None):


        # check if X is pandas DataFrame
        if not isinstance(X, pd.DataFrame):
            raise ValueError("Input data is no valid pandas DataFrame")
                     
        transformed_data = X
        
        if self.max_power in X.columns:
        # initialize transformed data.
            transformed_data.loc[:, self.current_power] = \
                transformed_data.loc[:, self.current_power] / transformed_data.loc[:, self.max_power]
        else:
            print('WARNING:', self.max_power, 'not in columns of dataframe:', X.columns)
            
        return transformed_data
