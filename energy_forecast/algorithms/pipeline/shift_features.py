import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin

from pies.timeseries import SlidingWindow

class ShiftFeatures(BaseEstimator, TransformerMixin):


    def __init__(self, sliding_window_features = ['AirPressure', 'Temperature', 'Humidity', 'WindSpeed100m',
            'WindSpeed10m', 'WindDirectionZonal100m', 'WindDirectionMeridional100m',
            'PowerGeneration'], window_size = 3):

        self.sliding_window_features = sliding_window_features
        self.window_size = window_size

    def fit(self, X, y=None):

        # check if X is pandas DataFrame
        if not isinstance(X, pd.DataFrame):
            raise ValueError("Input data is no valid pandas DataFrame")
        return self

    def transform(self, X, y=None):


        # check if X is pandas DataFrame
        if not isinstance(X, pd.DataFrame):
            raise ValueError("Input data is no valid pandas DataFrame")

        # initialize transformed data.
        df_transformed = X

        df_transformed = df_transformed.reset_index().set_index(['Day', 'Hour'])

        
        remaining_features = [c for c in df_transformed.columns if c not in self.sliding_window_features]

        sw = SlidingWindow(window_size=self.window_size, step_size=1, eval_position='past', timeseries_key_name='TimeUTC', dropna=True)
        sliding_window_df = sw.transform(df_transformed.loc[:, self.sliding_window_features])

        df1 = sliding_window_df
        df2 = df_transformed.loc[:, remaining_features]
        df2 = df2.iloc[2:,]
        df2 = df2.reset_index()
        df1 = df1.reset_index()
        df1['TimeUTC'] = df2.TimeUTC
        df1 = df1.set_index('TimeUTC')
        df2 = df2.set_index('TimeUTC')
        df2 = df2.drop(['Day', 'Hour'], axis=1)

        df_merged = df1.join(df2)

        return df_merged
