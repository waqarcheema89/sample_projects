import numpy as np
import pandas as pd

from sklearn.base import BaseEstimator, TransformerMixin


class FilterFeatureSelection(BaseEstimator,TransformerMixin):
    
    def __init__(self, list_scores = [], selection_method = 'averageFull'):
        self.list_scores = list_scores
        self.selection_method = selection_method
        
    def fit(self, X, y=None):
 
        if not isinstance(X, list):
            raise ValueError("Input data is no valid pandas DataFrame")
        return self
    
    def transform(self, X, y=None):
        
        if not isinstance(X, list):
            raise ValueError("Input data is no valid pandas DataFrame")
        
        list_features = np.zeros([len(X), len(self.list_scores), len(X[0].columns)-1])
        
        for idx, wp in enumerate(X):
            for idx2, score in enumerate(self.list_scores):
                if score.__name__ in ['fisher_score', 'reliefF']:
                    score_vals = self._score_farm(wp, score)
                    fs = np.argsort(score_vals)[::-1]
                elif score.__name__ in ['mrmr','mim', 'mifs']:
                    fs = self._score_farm_it(wp, score)
                else:
                    print(score.__name__)
                    raise ValueError('Invalid scoring function, must be one of: fisher_score, reliefF, mrmr, mim, mifs')
                list_features[idx, idx2, :] = fs
            print('WP no.'+str(idx)+' DONE!')
        bfs = self._best_features(list_features)
        return [X, bfs]
        
    def _best_features(self, list_features, method='averageFull'):
        if list_features == []:
            return []
        if method == 'averageFull' or method == 'medianFull':
            lf_reshaped = list_features.reshape(-1,list_features.shape[2])
            ranks = np.zeros(lf_reshaped.shape)
            for idx in range(lf_reshaped.shape[0]):
                ranks[idx,:] = np.argsort(lf_reshaped[idx,:])
            if method == 'averageFull':
                return np.argsort(np.mean(ranks,axis=0))
            elif method == 'medianFull':
                return np.argsort(np.median(ranks,axis=0))
        elif method == 'averageSingle' or method == 'medianSingle':
            lf_reshaped = list_features
            ranks = {}
            for idx in range(len(list_features.shape[0])):
                ranks[idx] = np.zeros[list_features.shape[1], list_features.shape[2]]
                for idx2 in range(list_features.shape[1]):
                    ranks[idx][:,idx2] = np.argsort(lf_reshaped[idx, :, idx2])
                if method == 'averageSingle':
                    ranks[idx] = np.mean(ranks[idx],axis=1)
                elif method == 'medianSingle':
                    ranks[idx] = np.median(ranks[idx],axis=1)
            return np.argsort(ranks)
        
    def _score_farm(self, df, score_function):

        cur_features  = list(df.columns)
        cur_features.remove('PowerGeneration')

        cur_score = score_function(df.loc[:,cur_features].values, df.loc[:,'PowerGeneration'].values)

        return cur_score

    def _score_farm_it(self, df, score_function):

        cur_features  = list(df.columns)
        cur_features.remove('PowerGeneration')

        F, J_CMI, MIfy = score_function(df.loc[:,cur_features].values, df.loc[:,'PowerGeneration'].values)

        return F
    