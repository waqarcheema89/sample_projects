from sklearn.base import BaseEstimator, TransformerMixin
import algorithms.utils.read_data as rd
from algorithms.utils.data import extend_list
from algorithms.utils.data import reset_index
import numpy as np
import pandas as pd

class ReadPowerData(BaseEstimator, TransformerMixin):


    def __init__(self, use_metadata = False, use_correction = False):
        self.use_metadata = use_metadata
        self.use_correction = use_correction

    def fit(self, X, y=None):

        # check if X is pandas DataFrame
        if not isinstance(X, str):
            raise ValueError("Input data is no valid string")
        return self



    def transform(self, X, y=None):


        # check if X is String
        if not isinstance(X, str):
            raise ValueError("Input data is no valid String")

        path = X
            
        X = rd.extend_home_path(X)

        power = rd.get_power_data_from_hdf(X)
        
        if self.use_metadata == True:
            length = len(power.iloc[:, 0])
            metadata = rd.get_meta_data_from_hdf(X)

            for feat_name in metadata.columns:
                feat = metadata[feat_name].values
                if feat.dtype=='object':
                    if not pd.isnull(feat):
                        power[feat_name] = extend_list(feat, length)
                else:
                    if not np.isnan(feat):
                        power[feat_name] = extend_list(feat, length)
                    
        
#         elevation = metadata['Elevation'].values[0]
#         if not np.isnan(elevation):
#             power['Elevation'] = extend_list(elevation, length)

#         hub_height = metadata['HubHeight'].values[0]
#         if not pd.isnull(hub_height):
#             power['HubHeight'] = extend_list(hub_height, length)

#         max_diameter = metadata['MaxDiameter'].values[0]
#         if not np.isnan(max_diameter):
#             power['MaxDiameter'] = extend_list(max_diameter, length)

        power = reset_index(power)
        power.set_index('TimeUTC', inplace=True)
        
        if self.use_correction == True:
            last_occ = path.rfind('/')
            mask_file_name = path[:last_occ+1]+'masks/mask_'+ path[last_occ+1:-2]+'csv'
            mask = pd.read_csv(mask_file_name, header=None)
            if not any(mask.values==1):
                raise ValueError('Blacklisted windparks have empty mask!')
            else:
                power = pd.DataFrame(power[mask.values == 1])

        return power
