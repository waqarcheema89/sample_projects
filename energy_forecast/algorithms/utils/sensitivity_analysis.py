import pandas as pd

def sobol_res_to_df(sobol_res, problem):
    import pandas as pd
    res = pd.DataFrame({'S1':sobol_res['S1'], \
                  'S1_conf':sobol_res['S1_conf'], \
                 'ST':sobol_res['ST'],\
                 'ST_conf':sobol_res['ST_conf']})

    res.index = problem['names']

    s2 = pd.DataFrame(sobol_res['S2'], columns= problem['names'], index=problem['names'])
    s2_conf = pd.DataFrame(sobol_res['S2_conf'], columns= problem['names'], index=problem['names'])

    return res, s2, s2_conf


def morris_res_to_df(morris_res):
    import pandas as pd
    morris_res = pd.DataFrame({'mu': morris_res['mu'], \
                          'sigma': morris_res['sigma'], \
                          'mu_star': morris_res['mu_star'], \
                          'mu_star_conf': morris_res['mu_star_conf']}, 
                          index=morris_res['names'] )
    return morris_res
