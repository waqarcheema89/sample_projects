from sklearn.model_selection import train_test_split
import numpy as np
from algorithms.utils.data import reset_index


def split_df_for_experiment(df, train_percent=.6, validate_percent=.2, seed=42, shuffle=False):
    np.random.seed(seed)
    if shuffle:
        perm = np.random.permutation(df.index)
    else:
        perm = df.index
        
    m = len(df.index)
    train_end = int(train_percent * m)
    validate_end = int(validate_percent * m) + train_end
    train = df.loc[perm[:train_end]]
    validate = df.loc[perm[train_end:validate_end]]
    test = df.loc[perm[validate_end:]]
    return train, validate, test
