import itertools


def flatten_list(list):
    return [item for sublist in list for item in sublist]



def reset_index(df):
    df = df.reset_index()
    df.drop('index', axis=1, inplace=True)
    return df

def find_subsets(S,m):
    return set(itertools.combinations(S,m))

def is_subset(subset, complete_set):
    return all(s in complete_set for s in subset)