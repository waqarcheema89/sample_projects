import os
import numpy as np
import pandas as pd
from algorithms.utils.pandas import convert_string_col_to_date
from algorithms.utils.data import extend_list
from algorithms.utils.data import reset_index


def get_meta_data_from_hdf(file_name):

    with pd.HDFStore(file_name) as store:
        df = store['metadata']
        df['Elevation'] = np.array(df['Elevation'])
        df['HubHeight'] = np.array(df['HubHeight'])
        df['MaxDiameter'] = np.array(df['MaxDiameter'])

    return df


def find_files(directory,
               suffix=".csv",
               file_filter=[]):

    if '~' in directory:
        directory = os.path.expanduser(directory)

    file_names = np.array([])
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(suffix):
                if len(file_filter) != 0:
                    if all(s not in file for s in file_filter):
                        file_names = np.append(file_names, (root + '/' + file))
                else:
                    file_names = np.append(file_names, (root + '/' + file))
    return file_names


def get_power_data_from_hdf(file_name, time_col_name = 'TimeUTC'):

    with pd.HDFStore(file_name) as store:
        df = store['powerdata']

    store.close()

    convert_string_col_to_date(df, time_col_name)

    return df


def extend_home_path(path):
    if '~' in path:
        path = os.path.expanduser(path)
    if '~' in path:
        path = os.path.expanduser(path)

    return path


def get_power_data_with_meta_data_from_hdf(file_name):
    power = get_power_data_from_hdf(file_name)
    length = len(power.iloc[:, 0])
    metadata = get_meta_data_from_hdf(file_name)

    power['Elevation'] = extend_list(metadata['Elevation'].values[0], length)
    power['HubHeight'] = extend_list(metadata['HubHeight'].values[0], length)
    power['MaxDiameter'] = extend_list(
        metadata['MaxDiameter'].values[0], length)

    power = reset_index(power)

    return power


def read_and_combine(list_of_files):
    train_c, val_c, test_c = pd.DataFrame(), pd.DataFrame(), pd.DataFrame()
    for file in list_of_files:
        df = get_power_data_with_meta_data_from_hdf(file)

        train, val, test = pp.do_baseline_preprocessing(df, seed=SEED)

        train_c = train_c.append(train)
        val_c = val_c.append(val)
        test_c = test_c.append(test)

    return train_c, val_c, test_c
