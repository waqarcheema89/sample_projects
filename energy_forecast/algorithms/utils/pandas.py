import pandas as pd


def convert_date_col_to_string(df, col_name):



    df[col_name] = pd.to_datetime(
        df[col_name], utc=True).dt.strftime('%Y-%m-%dT%H:%M:%S')


def convert_string_col_to_date(df, col_name):


    if type(df.loc[:, col_name][0]) is not pd.Timestamp \
            and '0000-' in df.loc[:, col_name][0]:
        df.loc[:, col_name].replace({'0000-': '2013-'}, inplace=True, regex=True)
        df.loc[:, col_name].replace({'0001-': '2014-'}, inplace=True, regex=True)

    df.loc[:, col_name] = pd.to_datetime(df.loc[:, col_name], infer_datetime_format=True)
    # df.loc[:, col_name] = pd.to_datetime(df.loc[:, col_name])
    

def display_df(df):
    from IPython.display import display, HTML
    display(HTML(df.to_html()))
