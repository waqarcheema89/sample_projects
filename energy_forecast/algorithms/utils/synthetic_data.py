import numpy as np


def sinus_cosinus(nr_data_points=2000, random_seed=42, scale=True):
    np.random.seed(random_seed)

    # creating sinus shaped training data
    x = np.round((np.random.random((nr_data_points, 1))) - 0.5, 2)
    y = np.cos(20 * x) / np.exp(x**2 * 10) + \
        (np.sin(20 * x) + 1) * np.random.random(x.shape) * 0.2
    x = x.reshape(nr_data_points, 1)
    y = y.reshape(nr_data_points, 1)

    if scale:
        from sklearn import preprocessing
        min_max_scaler = preprocessing.MinMaxScaler()
        x = min_max_scaler.fit_transform(x)
        y = min_max_scaler.fit_transform(y)

    return x.astype(dtype=np.float32), y.astype(dtype=np.float32)


def g_function(values, a=[0, 1, 4.5, 9, 99, 99, 99, 99]):
    # Non-monotonic Sobol G Function (8 parameters)
    # First-order indices:
    # x1: 0.7165
    # x2: 0.1791
    # x3: 0.0237
    # x4: 0.0072
    # x5-x8: 0.0001

    if type(values) != np.ndarray:
        raise TypeError("The argument `values` must be a numpy ndarray")

    ltz = np.array(values) < 0
    gto = np.array(values) > 1

    if ltz.any() == True:
        raise ValueError("Sobol G function called with values less than zero")
    elif gto.any() == True:
        raise ValueError(
            "Sobol G function called with values greater than one")

    Y = np.zeros([values.shape[0]])

    for i, row in enumerate(values):
        Y[i] = 1.0

        for j in range(len(a)):
            x = row[j]
            Y[i] *= (abs(4 * x - 2) + a[j]) / (1 + a[j])

    return Y

