import os
from collections import defaultdict
import googlemaps
import numpy as np
import responses
from dateutil import parser as dp
import pandas as pd

from util import *

def convert_all_cosmo_dates(df):
    convert_row_to_date(df, 'ModelRunTimeUTC')
    convert_row_to_date(df, 'PredictionTimeUTC')

    
def convert_row_to_date(df, row_name):
    if type(df[row_name][0]) is str:
        res = []
        for d in df[row_name]:
            if d != '':
                res.append(dp.parse(d))
            else:
                res.append(np.nan)
        df[row_name] = res


def add_start_end_date_to_metadata(metadata, data_folder_raw_data):
    metadata['Start'] = ''
    metadata['End'] = ''
    rows_to_remove = []
    for i in range(0, len(metadata)):
        cur_name = data_folder_raw_data + metadata.Name[i] + '/synchronized_rawData_unfiltered.csv'
        if os.path.exists(cur_name):
            df = pd.read_csv(cur_name, sep=';')
            metadata.set_value(i, 'Start', df['currentGrib.dateTime'][0])
            metadata.set_value(i, 'End', df['currentGrib.dateTime'][len(df['currentGrib.dateTime']) - 1])

        else:
            print(metadata.Name[i] + ' could not be found in synchronized data folder.\n' + \
                  'Removing from data.')
            rows_to_remove.append(i)

    metadata.drop(rows_to_remove, inplace=True)
    convert_row_to_date(metadata, 'Start')
    convert_row_to_date(metadata, 'End')


def get_elevation(locations):
    key = 'AIzaSyC03nPJ6DVM1ExZnEagytVm8x32s9PKskQ'
    gmapsClient = googlemaps.Client(key=key)

    responses.add(responses.GET,
                  'https://maps.googleapis.com/maps/api/elevation/json',
                  body='{"status":"OK","results":[]}',
                  status=200,
                  content_type='application/json')

    elevations = gmapsClient.elevation(locations)

    return [evt['elevation'] for evt in elevations]


def get_metadata(wind_facilities):
    latitudes = []
    longitudes = []
    base_names = []
    max_diameters = []
    hub_heights = []
    timezones = []

    for wind_facility in wind_facilities:
        df = pd.read_csv(wind_facility, sep=';')

        longitudes.append(df.longitude[0])
        latitudes.append(df.latitude[0])
        base_names.append(df.name[0])
        max_diameters.append(df.maxdiameter[0])
        timezones.append(df.timezone[0])
        hub_heights.append(df.hubheight[0])

    elevations = get_elevation(list(zip(latitudes, longitudes)))

    return pd.DataFrame({'Name': base_names,
                         'Latitude': latitudes,
                         'Longitude': longitudes,
                         'Elevation': elevations,
                         'HubHeight': hub_heights,
                         'Timezone': timezones,
                         'MaxDiameter': max_diameters})


def get_dict_of_cosmo_files(cosmo_files):
    cosmo_files_structured = defaultdict(list)

    for cur_file in cosmo_files:
        file = open(cur_file)
        cur_base_name = file.readlines()[7][11:].rstrip()[:-4]
        file.close()

        cosmo_files_structured[cur_base_name].append(cur_file)

    return cosmo_files_structured


def find_matching_names(base_data_names, cosmo_file_names):
    # assure that name starts with wp_
    add_wp = lambda name: name if name.startswith('wp_') else  'wp_' + name
    # replace the chars that were modified by iwes
    replace_special_chars = lambda name: name.replace('-', '').replace(' ', '_')

    matching_names = []
    for key in cosmo_file_names:
        key_found = False
        adapted_cosmo_name = add_wp(key)
        for orig_name in base_data_names:
            adapted_name = replace_special_chars(add_wp(orig_name))
            if adapted_cosmo_name in adapted_name:
                key_found = True
                matching_names.append((orig_name, key))
                break
        # print file names that could not be matched
        if not key_found:
            print(key + ' could not be matched or is missing.')

    return matching_names


def read_cosmo_file(file_name):
    df_cosmo = pd.read_csv(file_name, sep=';', skiprows=34)
    rename_cosmo_cols(df_cosmo)
    convert_all_cosmo_dates(df_cosmo)

    return df_cosmo


def get_cosmo_col_names():

    return [
        # model_run [UTC]
        'ModelRunTimeUTC',
        # prediction_time [UTC]
        'PredictionTimeUTC',
        # what is the meaning of the numbers?
        # Is HL equal to HeightLevel???
        # U_GVL_47_HL []
        'ZonalWind122m',
        # V_GVL_47_HL []
        'MeridionalWind122m',
        # U_GVL_48_HL []
        'ZonalWind73m',
        # V_GVL_48_HL []
        'MeridionalWind73m',
        # U_GVL_49_HL []
        'ZonalWind36m',
        # V_GVL_49_HL []
        'MeridionalWind36m',
        # T_GVL_47_HL []
        'Temperature122m',
        # T_GVL_49_HL []
        'Temperature36m',
        # RELHUM_HAG_2_M []
        # What is HAG? HeightAboveGround
        'RelativeHumidity2M',
        # PS_SFC_0_M []
        'UnreducedGroundPressure0M',
        # PRMSL_MSL_0_M []
        # Is this correct?
        # 'MeanSeaLevelPressure_0M',
        'PressureReduced',
        # Z_IBHPA_500_HPA []
        'Geopotential',
        # 'TP_SFC_0_M []
        'TotalPrecipitation',
        # RSN_SFC_0_M []
        'SnowDensity',
        # SDWE_SFC_0_M []
        'SnowDepthWater'
    ]

def rename_cosmo_cols(df):

    df.columns = get_cosmo_col_names()

    return df


def read_raw_data_wp(file_name):
    df_raw_data = pd.read_csv(file_name, sep=';')
    convert_row_to_date(df_raw_data, 'currentGrib.dateTime')
    converted_raw_data = pd.DataFrame({'TimeUTC': df_raw_data['currentGrib.dateTime'],
                                       'AirPressure': df_raw_data['currentGrib.airPressure'],
                                       'Temperature': df_raw_data['currentGrib.temperature'],
                                       'Humidity': df_raw_data['currentGrib.humidity'],
                                       'WindSpeed100m': df_raw_data['currentGrib.upperWindSpeed'],
                                       'WindSpeed10m': df_raw_data['currentGrib.lowerWindSpeed'],
                                       'WindDirectionMeridional100m': df_raw_data['currentGrib.upperWindDirectionCosMath'],
                                       'WindDirectionZonal100m': df_raw_data['currentGrib.upperWindDirectionSinMath'],
                                       'PowerGeneration': df_raw_data['powerData.powerWatt'],
                                       'MaxPowerGeneration': df_raw_data['powerData.maxPowerWatt'],
                                       })
    return converted_raw_data


def create_hdf_for_file(cur_matching_names):
    #cur_matching_names = matching_names[0]
    store = pd.HDFStore(dest_folder + '/' + cur_matching_names[0] + '.h5')
    cur_metadata = metadata[metadata.Name == cur_matching_names[0]]

    for i in range(1, 21):
        s = '_e' + ('%02d.csv' % i)
        cur_cosmo_file = data_folder_cosmo + cur_matching_names[1] + s
        df = read_cosmo_file(cur_cosmo_file)

        mask = np.logical_and(df['ModelRunTimeUTC'] >= cur_metadata.Start.values[0],
                              df['ModelRunTimeUTC'] <= cur_metadata.End.values[0])
        df = df[mask]
        # assure that index is correct, after data has been removed due to the initial date
        df = df.reset_index().drop('index', axis=1)

        convert_date_col_to_string(df, 'ModelRunTimeUTC')
        convert_date_col_to_string(df, 'PredictionTimeUTC')

        # make sure to store the columns in the data_columns attribute and
        # to have the table format to have in compatible with matlab, R, ...
        store.append('cosmo/e_%02d' % i, df, format='table', data_columns=df.columns)

        convert_string_col_to_date(df, 'ModelRunTimeUTC')
        convert_string_col_to_date(df, 'PredictionTimeUTC')

    first_date_cosmo = df['ModelRunTimeUTC'].iloc[0]

    # assure that start time of cosmo and power data are in sync
    cur_metadata.Start.values[0] = first_date_cosmo
    df = read_raw_data_wp(data_folder_raw_data + cur_matching_names[0] + '/synchronized_rawData_unfiltered.csv')
    mask = df.TimeUTC >= first_date_cosmo
    df = df[mask]

    # assure that index is correct, after data has been removed due to the initial date
    df = df.reset_index().drop('index', axis = 1)
    # store final data

    convert_date_col_to_string(df, 'TimeUTC')

    store.put('powerdata', df, format='table', data_columns=df.columns)
    convert_date_col_to_string(cur_metadata, 'Start')
    convert_date_col_to_string(cur_metadata, 'End')
    store.put('metadata', cur_metadata, format='table', data_columns=cur_metadata.columns)
    convert_string_col_to_date(cur_metadata, 'Start')
    convert_string_col_to_date(cur_metadata, 'End')

    print(store)
    store.close()

def main_wp():
    # set-up data structure, change if required
    global data_folder_cosmo, data_folder_wind_facilities, data_folder_raw_data, dest_folder
    data_folder_cosmo = os.path.expanduser('~/data/WindSandbox2017/COSMO-DE-EPS/')
    data_folder_wind_facilities = os.path.expanduser('~/data/WindSandbox2017/ec_sandbox2017_wind_facilities/')
    data_folder_raw_data = os.path.expanduser('~/data/WindSandbox2017/snap2017eur/')
    dest_folder = os.path.expanduser('~/data/WindSandbox2017/')

    # read all kind of data
    cosmo_files = find_files(data_folder_cosmo)
    wind_power_files = find_files(data_folder_wind_facilities,
                                  file_filter=['nominal_capacities.csv', 'wind_facility.csv'])
    wind_facility_files = find_files(data_folder_wind_facilities,
                                     file_filter=['nominal_capacities.csv', 'wind_power_data.csv'])
    nominal_capacity_files = find_files(data_folder_wind_facilities,
                                        file_filter=['wind_power_data.csv', 'wind_facility.csv'])

    raw_data_files = find_files(data_folder_raw_data)
    cosmo_files_structured = get_dict_of_cosmo_files(cosmo_files)

    global metadata
    metadata = get_metadata(wind_facility_files)
    add_start_end_date_to_metadata(metadata, data_folder_raw_data)

    cosmo_files_structured = get_dict_of_cosmo_files(cosmo_files)

    base_data_names = list(metadata.Name.values)
    cosmo_file_names = list(cosmo_files_structured.keys())

    matching_names = find_matching_names(base_data_names, cosmo_file_names)

    for cur_matching_name in matching_names:
        create_hdf_for_file(cur_matching_name)

def nans(shape, dtype=float):
    a = np.empty(shape, dtype)
    a.fill(np.nan)
    return a

def main_old_wp():
    """ converts the old sandboxes in hdf5 files similar to the sandbox 2017 """
    data_folder_2013 = os.path.expanduser('~/data/sandboxDirty/sandbox2013/')
    data_folder_2014 = os.path.expanduser('~/data/sandboxDirty/sandbox2014/')
    dest_folder = os.path.expanduser('~/data/sandboxDirty/')

    raw_data_files_2013 = find_files(data_folder_2013)
    raw_data_files_2014 = find_files(data_folder_2014)

    nr_files = len(raw_data_files_2013)
    for i in range(nr_files):
        base_name = raw_data_files_2013[i].split('/')[-2]

        df_2013 = read_raw_data_wp(raw_data_files_2013[i])
        df_2014 = read_raw_data_wp(raw_data_files_2014[i])

        df_comb = concat_data_frames([df_2013, df_2014])
        convert_date_col_to_string(df_comb, 'TimeUTC')


        cur_metadata = pd.DataFrame({'Name': [base_name],
                                 'Latitude': [np.NaN],
                                 'Longitude': [np.NaN],
                                 'Elevation': [np.NaN],
                                 'HubHeight': [np.NaN],
                                 'MaxDiameter': [np.NaN],
                                 'Start': [df_comb.TimeUTC.iloc[0]],
                                 'End': [df_comb.TimeUTC.iloc[-1]]})

        store = pd.HDFStore(dest_folder + base_name + '.h5')

        store.put('powerdata', df_comb, format='table', data_columns=df_comb.columns)
        store.put('metadata', cur_metadata, format='table', data_columns=cur_metadata.columns)

        cn = get_cosmo_col_names()

        # create empty table for cosmo group
        d = np.array([[np.nan] for e in cn])
        for j in range(0, 21):
            # Write empty data set
            store.put('cosmo/e_%02d' % j, pd.DataFrame(d.T, columns=cn), format='table', data_columns=cn)

        store.close()


if __name__ == "__main__":
    main_old_wp()
