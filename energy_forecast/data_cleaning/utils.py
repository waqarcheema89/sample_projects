import os
import tarfile
import numpy as np
import zipfile
import pandas as pd

def unpack_compressed_file(file_name, destination):
    if file_name.endswith('.zip'):
        file = zipfile.ZipFile(file_name )
        file.extractall(destination)
        file.close()
    elif file_name.endswith('tar.gz'):
        file = tarfile.open(file_name, 'r:*' )
        file.extractall(destination)
        file.close()
    elif file_name.endswith('.gz'):
        import gzip
        with gzip.open(file_name, 'rb') as infile:
            with open(destination, 'wb') as outfile:
                for line in infile:
                    outfile.write(line)

def find_files(directory,
               suffix=".csv",
               file_filter=[]):

    if '~' in directory:
        directory = os.path.expanduser(directory)

    file_names = np.array([])
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(suffix):
                if len(file_filter) != 0:
                    if all(s not in file for s in file_filter):
                        file_names = np.append(file_names, (root + '/' + file))
                else:
                    file_names = np.append(file_names, (root + '/' + file))
    return file_names                    


def concat_data_frames(dataframes):
    """
    Abstration for common concatenation behaviour, where each data frame
    is appended at the end of its predecessor. Index is reseted.
    :param dataframes:
    :return: Concatenaded dataframes.
    """
    if type(dataframes) is list:
        df_comb = pd.concat(dataframes).reset_index().drop('index', axis = 1)
        return df_comb
    else:
        return None


def convert_date_col_to_string(df, col_name):
    """
    Converts in-place!
    :param df:
    :param col_name: to be transformed
    :return: NONE
    """

    df[col_name] = pd.to_datetime(df[col_name], utc=True).dt.strftime('%Y-%m-%dT%H:%M:%S')


def convert_string_col_to_date(df, col_name):
    """
    Converts in-place!
    :param df:
    :param col_name: to be transformed
    :return:
    """
    df.loc[:, col_name] = pd.to_datetime(df.loc[:, col_name])
